package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullTaskException;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator,
                              final String userId) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator, userId);
    }

    @Override
    public Task findOneByIndex(final Integer index,
                               final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        Task task = taskRepository.findOneByIndex(index, userId);
        if (task == null) throw new NullTaskException();
        return task;
    }

    @Override
    public Task findOneByName(final String name,
                              final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = taskRepository.findOneByName(name, userId);
        if (task == null) throw new NullTaskException();
        return task;
    }

    @Override
    public void removeOneByIndex(final Integer index,
                                 final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        Task task = taskRepository.findOneByIndex(index, userId);
        if (task == null) throw new NullTaskException();
        remove(task);
    }

    @Override
    public void removeOneByName(final String name,
                                final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Task task = taskRepository.findOneByName(name, userId);
        if (task == null) throw new NullTaskException();
        remove(task);
    }

    @Override
    public Task add(final String name,
                    final String description) throws Exception {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public Task updateTaskById(final String id,
                               final String name,
                               final String description,
                               final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(id, userId);
        if (task == null) throw new NullTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index,
                                  final String name,
                                  final String description,
                                  final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index, userId);
        if (task == null) throw new NullTaskException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id,
                              final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        task.setDateStarted(dateStarted);
        return task;
    }

    @Override
    public Task startTaskByName(final String name,
                                final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        task.setDateStarted(dateStarted);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index,
                                 final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        task.setDateStarted(dateStarted);
        return task;
    }

    @Override
    public Task finishTaskById(final String id,
                               final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name,
                                 final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index,
                                  final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task updateTaskStatusById(final String id,
                                     final Status status,
                                     final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateTaskStatusByName(final String name,
                                       final Status status,
                                       final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateTaskStatusByIndex(final Integer index,
                                        final Status status,
                                        final String userId) throws Exception {
        if (index == null || index < 0) throw new IncorrectIndexException(index);
        final Task task = findOneByIndex(index, userId);
        if (task == null) throw new NullTaskException();
        task.setStatus(status);
        return task;
    }

    public void showTask(final Task task){
        System.out.println("[ID] " + task.getId());
        System.out.println("[NAME] " + task.getName());
        System.out.println("[DESCRIPTION] " + task.getDescription());
    }

}
