package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullProjectException;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator,
                                 final String userid) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator, userid);
    }

    @Override
    public Project findOneByIndex(final Integer index,
                                  final String userId) throws Exception {
       if (index == null || index < 0) throw new IncorrectIndexException(index);
       Project project = projectRepository.findOneByIndex(index, userId);
       if (project == null) throw new NullProjectException();
       return project;
    }

    @Override
    public Project findOneByName(final String name, final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findOneByName(name, userId);
        if (project == null) throw new NullProjectException();
        return project;
    }

    @Override
    public void removeOneByIndex(final Integer index,
                                 final String userId) throws Exception{
       if (index < 0) throw new IncorrectIndexException(index);
        Project project = projectRepository.findOneByIndex(index, userId);
        if (project == null) throw new NullProjectException();
        remove(project);
    }

    @Override
    public void removeOneByName(final String name,
                                final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findOneByName(name, userId);
        if (project == null) throw new NullProjectException();
        remove(project);
    }

    @Override
    public Project add(String name, String description,
                       final String userId) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public Project updateProjectById(final String id,
                                     final String name,
                                     final String description,
                                     final String userId) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findById(id, userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index,
                                        final String name,
                                        final String description,
                                        final String userId) throws Exception {
        final Project project = findOneByIndex(index, userId);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id,
                                    final String userId) throws Exception {
        final Project project = findById(id, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name,
                                      final String userId) throws Exception {
        final Project project = findOneByName(name, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        project.setDateStarted(dateStarted);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index,
                                       final String userId) throws Exception {
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        project.setDateStarted(dateStarted);
        return project;
    }

    @Override
    public Project finishProjectById(final String id,
                                     final String userId) throws Exception {
        final Project project = findById(id, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name,
                                       final String userId) throws Exception {
        final Project project = findOneByName(name, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index,
                                        final String userId) throws Exception {
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project updateProjectStatusById(final String id,
                                           final Status status,
                                           final String userId) throws Exception {
        final Project project = findById(id, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateProjectStatusByName(final String name,
                                             final Status status,
                                             final String userId) throws Exception {
        final Project project = findOneByName(name, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateProjectStatusByIndex(final Integer index,
                                              final Status status,
                                              final String userId) throws Exception {
        final Project project = findOneByIndex(index, userId);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    public void showProject(Project project) {
        if (project == null) return;
        System.out.println("[ID] " + project.getId());
        System.out.println("[NAME] " + project.getName());
        System.out.println("[DESCRIPTION] " + project.getDescription());
    }

}
