package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    @Override
    public E add(E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public E findById(final String id,
                      final String userId) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id, userId);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public void removeById(final String id,
                           final String userId) {
        if (id == null || id.isEmpty()) return;
        repository.removeById(id, userId);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

}
