package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommand();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandNames();

    Collection<String> getCommandsArgs();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

}
