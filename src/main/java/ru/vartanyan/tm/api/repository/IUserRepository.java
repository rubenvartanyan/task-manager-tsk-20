package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public interface IUserRepository {

    void clear();

    List<User> findAll();

    User add(final User user);

    void remove(final User user);

    void removeById(final String id);

    User findById(final String id);

    User findByLogin(final String login);

    void removeByLogin(final String login);

}
