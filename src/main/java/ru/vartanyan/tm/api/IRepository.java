package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll(final String userId);

    E add(final E entity);

    E findById(final String id, final String userId);

    void clear(final String userId);

    void removeById(final String id, final String userId);

    void remove(final E entity);

}
