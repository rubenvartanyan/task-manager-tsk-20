package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator, String userId);

    Project findOneByIndex(Integer index, String userId) throws Exception;

    Project findOneByName(String name, String userId) throws Exception;

    void removeOneByIndex(Integer index, String userId) throws Exception;

    void removeOneByName(String name, String userId) throws Exception;

    Project add(String name, String description, String userId) throws Exception;

    Project updateProjectById(String id, String name, String description, String userId) throws Exception;

    Project updateProjectByIndex(Integer index, String name, String description, String userId) throws EmptyNameException, Exception;

    Project startProjectById(String id, String userId) throws Exception;

    Project startProjectByName(String name, String userId) throws Exception;

    Project startProjectByIndex(Integer index, String userId) throws Exception;

    Project finishProjectById(String id, String userId) throws Exception;

    Project finishProjectByName(String name, String userId) throws Exception;

    Project finishProjectByIndex(Integer index, String userId) throws Exception;

    Project updateProjectStatusById(String id, Status status, String userId) throws Exception;

    Project updateProjectStatusByName(String name, Status status, String userId) throws Exception;

    Project updateProjectStatusByIndex(Integer index, Status status, String userId) throws Exception;

    void showProject(Project project);

}
