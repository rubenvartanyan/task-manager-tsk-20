package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator, final String userId);

    Task findOneByIndex(Integer index, final String userId) throws Exception;

    Task findOneByName(String name, final String userId) throws Exception;

    void removeOneByIndex(Integer index, final String userId) throws Exception;

    void removeOneByName(String name, final String userId) throws Exception;

    Task add(String name, String description) throws Exception;

    Task updateTaskById(String id, String name, String description, String userId) throws Exception;

    Task updateTaskByIndex(Integer index, String name, String description, String userId) throws Exception;

    Task startTaskById(String id, String userId) throws Exception;

    Task startTaskByName(String name, String userId) throws Exception;

    Task startTaskByIndex(Integer index, String userId) throws Exception;

    Task finishTaskById(String id, String userId) throws Exception;

    Task finishTaskByName(String name, String userId) throws Exception;

    Task finishTaskByIndex(Integer index, String userId) throws Exception;

    Task updateTaskStatusById(String id, Status status, String userId) throws Exception;

    Task updateTaskStatusByName(String name, Status status, String userId) throws Exception;

    Task updateTaskStatusByIndex(Integer index, Status status, String userId) throws Exception;

    void showTask (Task task) throws Exception;

}
