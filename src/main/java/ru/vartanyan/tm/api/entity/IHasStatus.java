package ru.vartanyan.tm.api.entity;

import ru.vartanyan.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
