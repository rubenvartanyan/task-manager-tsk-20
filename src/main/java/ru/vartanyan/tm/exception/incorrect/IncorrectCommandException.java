package ru.vartanyan.tm.exception.incorrect;

public class IncorrectCommandException extends Exception{

    public IncorrectCommandException(String command) throws Exception {
        super("Error! " + command + " is incorrect command...");
    }

}