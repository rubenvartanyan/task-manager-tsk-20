package ru.vartanyan.tm.exception.incorrect;

public class IsNotNumberException extends Exception{

    public IsNotNumberException(String number) throws Exception {
        super("Error! " + number + " is not number...");
    }

}
