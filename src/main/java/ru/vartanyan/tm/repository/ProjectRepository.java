package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.model.AbstractEntity;
import ru.vartanyan.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final Comparator<Project> comparator,
                                 final String userId) {
        final List<Project> list = new ArrayList<>();
        for (Project entity : entities) {
            if (userId.equals(entity.getUserId())) list.add(entity);
        }
        list.sort(comparator);
        return list;

    }

    @Override
    public Project findOneByIndex(final Integer index,
                                  final String userId) {
        final Project project = entities.get(index);
        if (project.getUserId().equals(userId)) return project;
        return null;
    }

    @Override
    public Project findOneByName(final String name,
                                 final String userId) {
        for (Project project: entities) {
            if (project.getName().equals(name) && project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    @Override
    public void removeOneByIndex(final Integer index,
                                 final String userId) {
        final Project project = findOneByIndex(index, userId);
        entities.remove(project);
    }

    @Override
    public void removeOneByName(final String name,
                                final String userId) {
        final Project project = findOneByName(name, userId);
        entities.remove(project);
    }

}
