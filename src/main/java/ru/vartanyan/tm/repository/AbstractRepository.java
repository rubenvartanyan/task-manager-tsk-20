package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll(final String userId) {
        final List<E> list = new ArrayList<>();
        for (E entity : entities) {
            if (userId.equals(entity.getUserId())) list.add(entity);
        }
        return list;
    }

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public E findById(final String id,
                      final String userId) {
        if (id == null || id.isEmpty()) return null;
        for (final E entity: entities) {
            if (entity == null) continue;
            if (id.equals(entity.getId()) && userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public void clear(final String userId) {
        entities.removeIf(entity -> userId.equals(entity.getUserId()));
    }

    @Override
    public void removeById(final String id,
                           final String userId) {
        final E entity = findById(id, userId);
        if (entity == null) return;
        entities.remove(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

}
