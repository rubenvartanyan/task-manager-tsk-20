package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(final Comparator<Task> comparator,
                              final String userId) {
        final List<Task> tasks = new ArrayList<>(entities);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId,
                                         final String userId) {
        final List<Task> listOfTask = new ArrayList<>();
        for (Task task: entities){
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId,
                                           final String userId) {
        for (Task task: entities) {
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) task.setProjectId(null);
        }
        return entities;
    }

    @Override
    public Task bindTaskByProjectId(final String projectId,
                                    final String taskId,
                                    final String userId) {
        final Task task = findById(taskId, userId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String projectId,
                                      final String taskId,
                                      final String userID) {
        final Task task = findById(taskId, userID);
        task.setProjectId(null);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index,
                               final String userId) {
        final Task task = entities.get(index);
        if (task.getUserId().equals(userId)) return task;
        return null;
    }

    @Override
    public Task findOneByName(final String name,
                              final String userId) {
        for (Task task: entities) {
            if (task.getName().equals(name) && task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    @Override
    public void removeOneByIndex(final Integer index,
                                 final String userId) {
        final Task task = findOneByIndex(index, userId);
        entities.remove(task);
    }

    @Override
    public Task removeOneByName(final String name,
                                final String userId) {
        final Task task = findOneByName(name, userId);
        entities.remove(task);
        return task;
    }

}
