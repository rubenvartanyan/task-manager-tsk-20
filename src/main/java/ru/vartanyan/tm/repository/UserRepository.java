package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepository implements IUserRepository {

    protected final List<User> list = new ArrayList<>();

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<User> findAll() {
        return list;
    }

    @Override
    public User add(final User user) {
        list.add(user);
        return user;
    }

    @Override
    public void remove(final User user) {
        list.remove(user);
    }

    @Override
    public void removeById(final String id) {
        final User user = findById(id);
        if (user == null) return;
        remove(user);
    }

    @Override
    public User findById(final String id) {
        for (User user : list) {
            if (id.equals(user.getId())) return user;
        }
        return null;

    }

    @Override
    public User findByLogin(final String login) {
        for (final User user: list) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public void removeByLogin(final String login) {
        final User user = findByLogin(login);
        remove(user);
    }

}
