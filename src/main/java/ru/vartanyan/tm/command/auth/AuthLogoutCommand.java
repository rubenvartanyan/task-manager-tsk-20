package ru.vartanyan.tm.command.auth;

import ru.vartanyan.tm.command.AbstractAuthCommand;

public class AuthLogoutCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
