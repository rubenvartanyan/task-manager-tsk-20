package ru.vartanyan.tm.comparator;

import ru.vartanyan.tm.api.entity.IHasDateStarted;

import java.util.Comparator;

public final class ComparatorByDateStarted implements Comparator<IHasDateStarted> {

    private static final ComparatorByDateStarted INSTANCE = new ComparatorByDateStarted();

    public ComparatorByDateStarted() {
    }

    public static ComparatorByDateStarted getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasDateStarted o1, final IHasDateStarted o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStarted().compareTo(o2.getDateStarted());
    }

}
